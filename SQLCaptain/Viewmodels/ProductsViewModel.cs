﻿using System.Collections.ObjectModel;
using System.Windows;
using System.ComponentModel;
using SQLCaptain.Views;
using System.Windows.Input;
using SQLCaptain.Models;
using System.Windows.Controls;
using System.Data;
using System.Windows.Media;
using System.Collections.Generic;
using System;
using System.Windows.Data;

namespace SQLCaptain.Viewmodels
{
    public class ProductsViewModel : INotifyPropertyChanged
    {

        public ICommand LoadProductsCommand { get; set; }
        public ICommand ButtonFilterCommand { get; set; }
        public ICommand ButtonAddRecordCommand { get; set; }
        public ICommand ButtonSendRecordCommand { get; set; }

        List<DBProductsModel> products_list = new List<DBProductsModel>();
        DBModel ConnectionModel = new DBModel();

        public string TextBoxFilterIDSource { get; set; }
        public string TextBoxFilterNameSource { get; set; }
        public string TextBoxFilterQuantitySource { get; set; }
        public string TextBoxFilterPriceSource { get; set; }

        //public string TextBoxAddRecordIDSource { get; set; }
        public string TextBoxAddRecordNameSource { get; set; }
        public string TextBoxAddRecordQuantitySource { get; set; }
        public string TextBoxAddRecordPriceSource { get; set; }

        public ProductsViewModel() {

            products_list = ConnectionModel.GetProducts(null,null,null,null,"product_name", "ASC");
            LoadProductsCommand = new RelayCommand(LoadProducts);
            LoadProductsCommand.Execute(null);

            ButtonFilterCommand = new RelayCommand(ButtonFilterFunc);
            ButtonAddRecordCommand = new RelayCommand(ButtonAddRecordFunc);
            ButtonSendRecordCommand = new RelayCommand(ButtonSendRecordFunc);
        }       

        public event PropertyChangedEventHandler PropertyChanged;

        private Grid _DynamicGrid = new Grid();

        public Grid DynamicGrid
        {
            get { return _DynamicGrid; }
            set
            {
                _DynamicGrid = value;
                OnPropertyChanged(DynamicGrid);
            }
        }

        protected void OnPropertyChanged(Grid DynamicGrid)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("DynamicGrid"));
            }

        }


        private RowDefinition gridRowAddProduct = new RowDefinition();
        TextBox TextBoxAddRecordID = new TextBox();
        TextBox TextBoxAddRecordName = new TextBox();
        TextBox TextBoxAddRecordQuantity = new TextBox();
        TextBox TextBoxAddRecordPrice = new TextBox();

        #region LoadProducts
        private void LoadProducts()
        {
            AnyCellActive = false;

            ColumnDefinition gridCol1 = new ColumnDefinition();
            ColumnDefinition gridCol2 = new ColumnDefinition();
            ColumnDefinition gridCol3 = new ColumnDefinition();
            ColumnDefinition gridCol4 = new ColumnDefinition();
            ColumnDefinition gridCol5 = new ColumnDefinition();
            ColumnDefinition gridCol6 = new ColumnDefinition();

            DynamicGrid.ColumnDefinitions.Add(gridCol1);
            DynamicGrid.ColumnDefinitions.Add(gridCol2);
            DynamicGrid.ColumnDefinitions.Add(gridCol3);
            DynamicGrid.ColumnDefinitions.Add(gridCol4);
            DynamicGrid.ColumnDefinitions.Add(gridCol5);
            DynamicGrid.ColumnDefinitions.Add(gridCol6);

            RowDefinition gridRowFilter = new RowDefinition();
            gridRowAddProduct = new RowDefinition();
            RowDefinition gridRowHead = new RowDefinition();
            gridRowFilter.Height = new GridLength(55);
            gridRowAddProduct.Height = new GridLength(0);
            gridRowHead.Height = new GridLength(55);
            DynamicGrid.RowDefinitions.Add(gridRowFilter);
            DynamicGrid.RowDefinitions.Add(gridRowAddProduct);
            DynamicGrid.RowDefinitions.Add(gridRowHead);

            #region Wiersz filtrowania
            TextBox TextBoxFilterID = new TextBox();
            TextBoxFilterID.SetBinding(TextBox.TextProperty, new Binding("TextBoxFilterIDSource"));
            TextBoxFilterID.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(TextBoxFilterID, 0);
            Grid.SetColumn(TextBoxFilterID, 0);

            TextBox TextBoxFilterName = new TextBox();
            TextBoxFilterName.SetBinding(TextBox.TextProperty, new Binding("TextBoxFilterNameSource"));
            TextBoxFilterName.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(TextBoxFilterName, 0);
            Grid.SetColumn(TextBoxFilterName, 1);

            TextBox TextBoxFilterQuantity = new TextBox();
            TextBoxFilterQuantity.SetBinding(TextBox.TextProperty, new Binding("TextBoxFilterQuantitySource"));
            TextBoxFilterQuantity.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(TextBoxFilterQuantity, 0);
            Grid.SetColumn(TextBoxFilterQuantity, 2);

            TextBox TextBoxFilterPrice = new TextBox();
            TextBoxFilterPrice.SetBinding(TextBox.TextProperty, new Binding("TextBoxFilterPriceSource"));
            TextBoxFilterPrice.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(TextBoxFilterPrice, 0);
            Grid.SetColumn(TextBoxFilterPrice, 3);

            Button ButtonFilter = new Button();
            ButtonFilter.Content = "Filtruj";
            Binding binding2 = new Binding();
            binding2.Path = new PropertyPath("ButtonFilterCommand"); //Name of the property in Datacontext
            ButtonFilter.SetBinding(Button.CommandProperty, binding2);

            Button ButtonAddRecord = new Button();
            ButtonAddRecord.Content = "Dodaj rekord";
            Binding bindingButtonAddRecord = new Binding();
            bindingButtonAddRecord.Path = new PropertyPath("ButtonAddRecordCommand");
            ButtonAddRecord.SetBinding(Button.CommandProperty, bindingButtonAddRecord);



            ButtonFilter.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(ButtonFilter, 0);
            Grid.SetColumn(ButtonFilter, 4);


            ButtonAddRecord.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(ButtonAddRecord, 0);
            Grid.SetColumn(ButtonAddRecord, 5);

            #endregion

            #region Wiersz dodawanie rekordu
            TextBoxAddRecordID = new TextBox();
            // TextBoxAddRecordID.SetBinding(TextBox.TextProperty, new Binding("TextBoxAddRecordIDSource"));
            TextBoxAddRecordID.IsEnabled = false;
            TextBoxAddRecordID.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(TextBoxAddRecordID, 1);
            Grid.SetColumn(TextBoxAddRecordID, 0);

            TextBoxAddRecordName = new TextBox();
            TextBoxAddRecordName.SetBinding(TextBox.TextProperty, new Binding("TextBoxAddRecordNameSource"));
            TextBoxAddRecordName.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(TextBoxAddRecordName, 1);
            Grid.SetColumn(TextBoxAddRecordName, 1);

            TextBoxAddRecordQuantity = new TextBox();
            TextBoxAddRecordQuantity.SetBinding(TextBox.TextProperty, new Binding("TextBoxAddRecordQuantitySource"));
            TextBoxAddRecordQuantity.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(TextBoxAddRecordQuantity, 1);
            Grid.SetColumn(TextBoxAddRecordQuantity, 2);

            TextBoxAddRecordPrice = new TextBox();
            TextBoxAddRecordPrice.SetBinding(TextBox.TextProperty, new Binding("TextBoxAddRecordPriceSource"));
            TextBoxAddRecordPrice.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(TextBoxAddRecordPrice, 1);
            Grid.SetColumn(TextBoxAddRecordPrice, 3);

            Button ButtonSendRecord = new Button();
            ButtonSendRecord.Content = "Dodaj";
            Binding bindingButtonSendRecord = new Binding();
            bindingButtonSendRecord.Path = new PropertyPath("ButtonSendRecordCommand");
            ButtonSendRecord.SetBinding(Button.CommandProperty, bindingButtonSendRecord);


            ButtonSendRecord.Margin = new Thickness(0, 10, 10, 10);
            Grid.SetRow(ButtonSendRecord, 1);
            Grid.SetColumn(ButtonSendRecord, 4);
            #endregion


            #region Wiersz nagłówków
            TextBlock txtBlock1 = new TextBlock();
            txtBlock1.Text = "ID";
            txtBlock1.Name = "product_id";
            txtBlock1.MouseLeftButtonDown += sortProductsBy;
            Grid.SetRow(txtBlock1, 2);
            Grid.SetColumn(txtBlock1, 0);
            TextBlock txtBlock2 = new TextBlock();
            txtBlock2.Text = "Nazwa";
            txtBlock2.Name = "product_name";
            txtBlock2.MouseLeftButtonDown += sortProductsBy;
            Grid.SetRow(txtBlock2, 2);
            Grid.SetColumn(txtBlock2, 1);
            TextBlock txtBlock3 = new TextBlock();
            txtBlock3.Text = "Ilość";
            txtBlock3.Name = "product_quantity";
            txtBlock3.MouseLeftButtonDown += sortProductsBy;
            Grid.SetRow(txtBlock3, 2);
            Grid.SetColumn(txtBlock3, 2);
            TextBlock txtBlock4 = new TextBlock();
            txtBlock4.Text = "Cena";
            txtBlock4.Name = "product_price";
            txtBlock4.MouseLeftButtonDown += sortProductsBy;
            Grid.SetRow(txtBlock4, 2);
            Grid.SetColumn(txtBlock4, 3);
            #endregion

            DynamicGrid.Children.Add(TextBoxFilterID);
            DynamicGrid.Children.Add(TextBoxFilterName);
            DynamicGrid.Children.Add(TextBoxFilterQuantity);
            DynamicGrid.Children.Add(TextBoxFilterPrice);
            DynamicGrid.Children.Add(ButtonFilter);
            DynamicGrid.Children.Add(ButtonSendRecord);

            DynamicGrid.Children.Add(TextBoxAddRecordID);
            DynamicGrid.Children.Add(TextBoxAddRecordName);
            DynamicGrid.Children.Add(TextBoxAddRecordQuantity);
            DynamicGrid.Children.Add(TextBoxAddRecordPrice);
            DynamicGrid.Children.Add(ButtonAddRecord);

            DynamicGrid.Children.Add(txtBlock1);
            DynamicGrid.Children.Add(txtBlock2);
            DynamicGrid.Children.Add(txtBlock3);
            DynamicGrid.Children.Add(txtBlock4);

            int i = 3;
            foreach (DBProductsModel Track in products_list)
            {

                RowDefinition gridRow = new RowDefinition();
                gridRow.Height = new GridLength(55);
                DynamicGrid.RowDefinitions.Add(gridRow);

                TextBlock TextBlockID = new TextBlock();               
                TextBlockID.Text = Track.product_id.ToString();

                Grid.SetRow(TextBlockID, i);
                Grid.SetColumn(TextBlockID, 0);

                TextBlock txtBlockName = new TextBlock();
                txtBlockName.Text = Track.product_name;
                txtBlockName.Name = 'N' + TextBlockID.Text;
                txtBlockName.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;

                Grid.SetRow(txtBlockName, i);
                Grid.SetColumn(txtBlockName, 1);

                TextBlock txtBlockQuantity = new TextBlock();
                txtBlockQuantity.Text = Track.product_quantity.ToString();
                txtBlockQuantity.Name = 'N' + TextBlockID.Text;
                txtBlockQuantity.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;

                Grid.SetRow(txtBlockQuantity, i);
                Grid.SetColumn(txtBlockQuantity, 2);

                TextBlock txtBlockPrice = new TextBlock();
                txtBlockPrice.Text = Track.product_price.ToString();
                txtBlockPrice.Text = txtBlockPrice.Text.Replace(',', '.');
                txtBlockPrice.Name = 'N' + TextBlockID.Text;
                txtBlockPrice.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;

                Grid.SetRow(txtBlockPrice, i);
                Grid.SetColumn(txtBlockPrice, 3);


                TextBlock deleteRecord = new TextBlock();
                deleteRecord.Text = "Usuń";
                deleteRecord.Name = 'N' + TextBlockID.Text;
                deleteRecord.MouseLeftButtonDown += deleteRecord_MouseLeftButtonDown;

                Grid.SetRow(deleteRecord, i);
                Grid.SetColumn(deleteRecord, 5);

                DynamicGrid.Children.Add(TextBlockID);
                DynamicGrid.Children.Add(txtBlockName);
                DynamicGrid.Children.Add(txtBlockQuantity);
                DynamicGrid.Children.Add(txtBlockPrice);
                DynamicGrid.Children.Add(deleteRecord);
                i++;


                //MessageBox.Show(Track.product_id.ToString() +" "+ Track.product_name + " " + Track.product_quantity.ToString() + " " + Track.product_price.ToString());

             
        }
       
      
        }
#endregion  

    string pretext;
        string prename;
        bool AnyCellActive=false;

        private void TxtBlockID_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2 && AnyCellActive==false)
            {
                TextBlock TextBlockID = sender as TextBlock;
                // MessageBox.Show(TextBlockID.Text);
               // MessageBox.Show(TextBlockID.Name);
              // MessageBox.Show(TextBlockID.Text+ Grid.GetRow(TextBlockID).ToString());

             TextBox txtBoxID = new TextBox();
             txtBoxID.Text = TextBlockID.Text;
             pretext = TextBlockID.Text;
             prename = TextBlockID.Name;
             txtBoxID.Height = 20;
                txtBoxID.HorizontalAlignment = HorizontalAlignment.Stretch;
                txtBoxID.VerticalAlignment = VerticalAlignment.Top;
                txtBoxID.Margin = new Thickness(0,0,0,0);
                txtBoxID.KeyDown += txtBoxIDID_KeyDown;
                // Grid.SetRow(null, Grid.GetRow(TextBlockID));
                // Grid.SetColumn(null, Grid.GetColumn(TextBlockID));
               // txtBoxID.Focus();
               // txtBoxID.Select(txtBoxID.Text.Length-1, txtBoxID.Text.Length);
                txtBoxID.Name = TextBlockID.Name;
                Grid.SetRow(txtBoxID, Grid.GetRow(TextBlockID));
             Grid.SetColumn(txtBoxID, Grid.GetColumn(TextBlockID));
             DynamicGrid.Children.Remove(TextBlockID);
             DynamicGrid.Children.Add(txtBoxID);

                AnyCellActive = true;   

            }
        }

        private void txtBoxIDID_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox TextBoxID = sender as TextBox;
            string id = TextBoxID.Name.Substring(1);

            if (e.Key == Key.Enter)
            {
                
                TextBlock TextBlockID = new TextBlock();
                TextBlockID.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;
                TextBlockID.Text = TextBoxID.Text;
                TextBlockID.Name = TextBoxID.Name;
                // TextBlockID.Height = 30;
                Grid.SetRow(TextBlockID, Grid.GetRow(TextBoxID));
                Grid.SetColumn(TextBlockID, Grid.GetColumn(TextBoxID));
                DynamicGrid.Children.Remove(TextBoxID);
                DynamicGrid.Children.Add(TextBlockID);

               // DBModel ConnectionModel = new DBModel();
                ConnectionModel.UpdateProductsCell(TextBlockID.Text, id , Grid.GetColumn(TextBoxID));
                AnyCellActive = false;
            }
            if (e.Key == Key.Escape)
            {
                
                TextBlock TextBlockID = new TextBlock();
                TextBlockID.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;
                TextBlockID.Text = pretext;
                TextBlockID.Name = prename;
                Grid.SetRow(TextBlockID, Grid.GetRow(TextBoxID));
                Grid.SetColumn(TextBlockID, Grid.GetColumn(TextBoxID));
                DynamicGrid.Children.Remove(TextBoxID);
                DynamicGrid.Children.Add(TextBlockID);
                AnyCellActive = false;
            }
        }

        private void deleteRecord_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TextBlock TextBlockEdit = sender as TextBlock;

            if (MessageBox.Show("Czy na pewno chcesz usunąć rekord o id " + TextBlockEdit.Name.Substring(1)+"?", "Czy na pewno?", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                                
                DBModel ConnectionModel = new DBModel();


                if (ConnectionModel.deleteProduct(TextBlockEdit.Name.Substring(1)) ==true)
                {
                    ButtonFilterFunc();
                    MessageBox.Show("Rekord został usunięty!");
                    
                }



            }
          
        }

        string checkSortHow="ASC";

        private void sortProductsBy(object sender, MouseButtonEventArgs e)
        {
            TextBlock TextBlock = sender as TextBlock;


            if (e.ClickCount == 2 )
            {
                switch (TextBlock.Name)
                {
                    case "product_id":
                        if (checkSortHow == "ASC")
                        {
                            products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource, TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource, "product_id", "DESC");
                            checkSortHow = "DESC";
                        }
                        else
                        {
                            products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource,TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource,"product_id", "ASC");
                            checkSortHow = "ASC";
                        }
                   
                        break;
                    case "product_name":
                        if (checkSortHow == "ASC")
                        {
                            products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource, TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource, "product_name", "DESC");
                            checkSortHow = "DESC";
                        }
                        else
                        {
                            products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource, TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource, "product_name", "ASC");
                            checkSortHow = "ASC";
                        }

                        break;
                    case "product_quantity":
                        if (checkSortHow == "ASC")
                        {
                            products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource, TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource, "product_quantity", "DESC");
                            checkSortHow = "DESC";
                        }
                        else
                        {
                            products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource, TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource, "product_quantity", "ASC");
                            checkSortHow = "ASC";
                        }

                        break;
                    case "product_price":
                        if (checkSortHow == "ASC")
                        {
                            products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource, TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource, "product_price", "DESC");
                            checkSortHow = "DESC";
                        }
                        else
                        {
                            products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource, TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource, "product_price", "ASC");
                            checkSortHow = "ASC";
                        }

                        break;
                    default:
                        MessageBox.Show("Ta funkcja jest jeszcze nie dostępna!");
                        break;

                }
                
                //LoadProductsCommand = new RelayCommand(LoadProducts);
                if (products_list != null)
                {
                    _DynamicGrid = null;
                    _DynamicGrid = new Grid();
                    LoadProductsCommand.Execute(null);
                    OnPropertyChanged(DynamicGrid);
                }
                
            }
        }

        private void ButtonFilterFunc()
        {
            
                products_list = ConnectionModel.GetProducts(TextBoxFilterIDSource, TextBoxFilterNameSource, TextBoxFilterQuantitySource, TextBoxFilterPriceSource, "product_id", "ASC");
                checkSortHow = "ASC";
          

            if (products_list != null)
            {
                _DynamicGrid = null;
                _DynamicGrid = new Grid();
                LoadProductsCommand.Execute(null);
                OnPropertyChanged(DynamicGrid);
            }
        }

        private void ButtonAddRecordFunc()
        {

           if(gridRowAddProduct.Height.ToString()=="0")
            gridRowAddProduct.Height = new GridLength(50);
           else
            gridRowAddProduct.Height = new GridLength(0);
         
        }

        private void ButtonSendRecordFunc()
        {
            if (!String.IsNullOrEmpty(TextBoxAddRecordNameSource) && !String.IsNullOrEmpty(TextBoxAddRecordQuantitySource) && !String.IsNullOrEmpty(TextBoxAddRecordPriceSource))
            {
                ConnectionModel.AddProduct(TextBoxAddRecordNameSource, TextBoxAddRecordQuantitySource, TextBoxAddRecordPriceSource);
                TextBoxAddRecordName.Text = null;
                TextBoxAddRecordQuantity.Text = null;
                TextBoxAddRecordPrice.Text = null;
                TextBoxAddRecordNameSource = null;
                TextBoxAddRecordQuantitySource = null;
                TextBoxAddRecordPriceSource = null;
                gridRowAddProduct.Height = new GridLength(0);

                ButtonFilterFunc();

                MessageBox.Show("Baza danych zauktualizowana!");
            }

            else
            {
                MessageBox.Show("Błędne dane!");
            }
                
        }

        


    }
}
