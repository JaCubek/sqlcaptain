﻿using System.Collections.ObjectModel;
using System.Windows;
using System.ComponentModel;
using SQLCaptain.Views;
using System.Windows.Input;
using SQLCaptain.Models;
using System.Windows.Controls;
using System.Data;
using System.Windows.Media;
using System.Collections.Generic;
using System;

namespace SQLCaptain.Viewmodels
{
    public class ClientsViewModel : INotifyPropertyChanged
    {

        public ICommand LoadClientsCommand { get; set; }

        public ClientsViewModel()
        {

            LoadClientsCommand = new RelayCommand(LoadClients);
            LoadClientsCommand.Execute(null);

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private Grid _DynamicGrid = new Grid();

        public Grid DynamicGrid
        {
            get { return _DynamicGrid; }
            set
            {
                _DynamicGrid = value;
                OnPropertyChanged(DynamicGrid);
            }
        }

        protected void OnPropertyChanged(Grid DynamicGrid)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("DynamicGrid"));
            }

        }



        private void LoadClients()
        {
            List<DBClientsModel> Clients_list = new List<DBClientsModel>();
            DBModel model = new DBModel();
            Clients_list = model.GetClients();



            ColumnDefinition gridCol1 = new ColumnDefinition();
            ColumnDefinition gridCol2 = new ColumnDefinition();
            ColumnDefinition gridCol3 = new ColumnDefinition();
            ColumnDefinition gridCol4 = new ColumnDefinition();
            DynamicGrid.ColumnDefinitions.Add(gridCol1);
            DynamicGrid.ColumnDefinitions.Add(gridCol2);
            DynamicGrid.ColumnDefinitions.Add(gridCol3);
            DynamicGrid.ColumnDefinitions.Add(gridCol4);
            RowDefinition gridRowHead = new RowDefinition();
            gridRowHead.Height = new GridLength(55);
            DynamicGrid.RowDefinitions.Add(gridRowHead);

            TextBlock txtBlock1 = new TextBlock();
            txtBlock1.Text = "ID";
            Grid.SetRow(txtBlock1, 0);
            Grid.SetColumn(txtBlock1, 0);
            TextBlock txtBlock2 = new TextBlock();
            txtBlock2.Text = "Imię";
            Grid.SetRow(txtBlock2, 0);
            Grid.SetColumn(txtBlock2, 1);
            TextBlock txtBlock3 = new TextBlock();
            txtBlock3.Text = "Adres";
            Grid.SetRow(txtBlock3, 0);
            Grid.SetColumn(txtBlock3, 2);
            TextBlock txtBlock4 = new TextBlock();
            txtBlock4.Text = "Numer tel.";
            Grid.SetRow(txtBlock4, 0);
            Grid.SetColumn(txtBlock4, 3);

            DynamicGrid.Children.Add(txtBlock1);
            DynamicGrid.Children.Add(txtBlock2);
            DynamicGrid.Children.Add(txtBlock3);
            DynamicGrid.Children.Add(txtBlock4);

            int i = 1;
            foreach (DBClientsModel Track in Clients_list)
            {

                RowDefinition gridRow = new RowDefinition();
                gridRow.Height = new GridLength(55);
                DynamicGrid.RowDefinitions.Add(gridRow);

                TextBlock TextBlockID = new TextBlock();
                TextBlockID.Text = Track.client_id.ToString();

                Grid.SetRow(TextBlockID, i);
                Grid.SetColumn(TextBlockID, 0);

                TextBlock txtBlockName = new TextBlock();
                txtBlockName.Text = Track.client_name;
                txtBlockName.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;

                Grid.SetRow(txtBlockName, i);
                Grid.SetColumn(txtBlockName, 1);

                TextBlock txtBlockQuantity = new TextBlock();
                txtBlockQuantity.Text = Track.client_address;
                txtBlockQuantity.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;

                Grid.SetRow(txtBlockQuantity, i);
                Grid.SetColumn(txtBlockQuantity, 2);

                TextBlock txtBlockPrice = new TextBlock();
                txtBlockPrice.Text = Track.client_phone;
                txtBlockPrice.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;

                Grid.SetRow(txtBlockPrice, i);
                Grid.SetColumn(txtBlockPrice, 3);


                DynamicGrid.Children.Add(TextBlockID);
                DynamicGrid.Children.Add(txtBlockName);
                DynamicGrid.Children.Add(txtBlockQuantity);
                DynamicGrid.Children.Add(txtBlockPrice);
                i++;


                //MessageBox.Show(Track.product_id.ToString() +" "+ Track.product_name + " " + Track.product_quantity.ToString() + " " + Track.product_price.ToString());


            }

            #region Dynamiczny Grid przykład    
            /*  DynamicGrid.Width = 800;
              DynamicGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
              DynamicGrid.VerticalAlignment = VerticalAlignment.Center;
              DynamicGrid.ShowGridLines = true;

              DynamicGrid.Background = new SolidColorBrush(Colors.LightSteelBlue);

              // Create Columns
              ColumnDefinition gridCol1 = new ColumnDefinition();
              ColumnDefinition gridCol2 = new ColumnDefinition();
              ColumnDefinition gridCol3 = new ColumnDefinition();
              DynamicGrid.ColumnDefinitions.Add(gridCol1);
              DynamicGrid.ColumnDefinitions.Add(gridCol2);
              DynamicGrid.ColumnDefinitions.Add(gridCol3);

              // Create Rows
              RowDefinition gridRow1 = new RowDefinition();
              gridRow1.Height = new GridLength(45);
              RowDefinition gridRow2 = new RowDefinition();
              gridRow2.Height = new GridLength(45);
              RowDefinition gridRow3 = new RowDefinition();
              gridRow3.Height = new GridLength(45);
              DynamicGrid.RowDefinitions.Add(gridRow1);
              DynamicGrid.RowDefinitions.Add(gridRow2);
              DynamicGrid.RowDefinitions.Add(gridRow3);

              // Add first column header
              TextBlock txtBlock1 = new TextBlock();
              txtBlock1.Text = "Author Name";
              txtBlock1.FontSize = 14;
              txtBlock1.FontWeight = FontWeights.Bold;
              txtBlock1.Foreground = new SolidColorBrush(Colors.Green);
              txtBlock1.VerticalAlignment = VerticalAlignment.Top;
              Grid.SetRow(txtBlock1, 0);
              Grid.SetColumn(txtBlock1, 0);

              // Add second column header
              TextBlock txtBlock2 = new TextBlock();
              txtBlock2.Text = "Age";
              txtBlock2.FontSize = 14;
              txtBlock2.FontWeight = FontWeights.Bold;
              txtBlock2.Foreground = new SolidColorBrush(Colors.Green);
              txtBlock2.VerticalAlignment = VerticalAlignment.Top;
              Grid.SetRow(txtBlock2, 0);
              Grid.SetColumn(txtBlock2, 1);

              // Add third column header
              TextBlock txtBlock3 = new TextBlock();
              txtBlock3.Text = "Book";
              txtBlock3.FontSize = 14;
              txtBlock3.FontWeight = FontWeights.Bold;
              txtBlock3.Foreground = new SolidColorBrush(Colors.Green);
              txtBlock3.VerticalAlignment = VerticalAlignment.Top;
              Grid.SetRow(txtBlock3, 0);
              Grid.SetColumn(txtBlock3, 2);

              //// Add column headers to the Grid
              DynamicGrid.Children.Add(txtBlock1);
              DynamicGrid.Children.Add(txtBlock2);
              DynamicGrid.Children.Add(txtBlock3);

              // Create first Row
              TextBlock authorText = new TextBlock();
              authorText.Text = "Mahesh Chand";
              authorText.FontSize = 12;
              authorText.FontWeight = FontWeights.Bold;
              Grid.SetRow(authorText, 1);
              Grid.SetColumn(authorText, 0);

              TextBlock ageText = new TextBlock();
              ageText.Text = "33";
              ageText.FontSize = 12;
              ageText.FontWeight = FontWeights.Bold;
              Grid.SetRow(ageText, 1);
              Grid.SetColumn(ageText, 1);

              TextBlock bookText = new TextBlock();
              bookText.Text = "GDI+ Programming";
              bookText.FontSize = 12;
              bookText.FontWeight = FontWeights.Bold;
              Grid.SetRow(bookText, 1);
              Grid.SetColumn(bookText, 2);
              // Add first row to Grid
              DynamicGrid.Children.Add(authorText);
              DynamicGrid.Children.Add(ageText);
              DynamicGrid.Children.Add(bookText);

              // Create second row
              authorText = new TextBlock();
              authorText.Text = "Mike Gold";
              authorText.FontSize = 12;
              authorText.FontWeight = FontWeights.Bold;
              Grid.SetRow(authorText, 2);
              Grid.SetColumn(authorText, 0);

              ageText = new TextBlock();
              ageText.Text = "35";
              ageText.FontSize = 12;
              ageText.FontWeight = FontWeights.Bold;
              Grid.SetRow(ageText, 2);
              Grid.SetColumn(ageText, 1);

              bookText = new TextBlock();
              bookText.Text = "Programming C#";
              bookText.FontSize = 12;
              bookText.FontWeight = FontWeights.Bold;
              Grid.SetRow(bookText, 2);
              Grid.SetColumn(bookText, 2);

              // Add second row to Grid
              DynamicGrid.Children.Add(authorText);
              DynamicGrid.Children.Add(ageText);
              DynamicGrid.Children.Add(bookText);*/
            #endregion
        }

        string pretext;

        private void TxtBlockID_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                TextBlock TextBlockID = sender as TextBlock;
                // MessageBox.Show(TextBlockID.Text);


                // MessageBox.Show(TextBlockID.Text+ Grid.GetRow(TextBlockID).ToString());

                TextBox txtBoxID = new TextBox();
                txtBoxID.Text = TextBlockID.Text;
                pretext = TextBlockID.Text;
                txtBoxID.Height = 20;
                txtBoxID.KeyDown += txtBoxIDID_KeyDown;
                // Grid.SetRow(null, Grid.GetRow(TextBlockID));
                // Grid.SetColumn(null, Grid.GetColumn(TextBlockID));
                Grid.SetRow(txtBoxID, Grid.GetRow(TextBlockID));
                Grid.SetColumn(txtBoxID, Grid.GetColumn(TextBlockID));
                DynamicGrid.Children.Remove(TextBlockID);
                DynamicGrid.Children.Add(txtBoxID);

            }
        }

        private void txtBoxIDID_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox TextBoxID = sender as TextBox;
            if (e.Key == Key.Enter)
            {

                TextBlock TextBlockID = new TextBlock();
                TextBlockID.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;
                TextBlockID.Text = TextBoxID.Text;
                // TextBlockID.Height = 30;
                Grid.SetRow(TextBlockID, Grid.GetRow(TextBoxID));
                Grid.SetColumn(TextBlockID, Grid.GetColumn(TextBoxID));
                DynamicGrid.Children.Remove(TextBoxID);
                DynamicGrid.Children.Add(TextBlockID);
                DBModel ConnectionModel = new DBModel();
                ConnectionModel.UpdateClientsCell(TextBlockID.Text, Grid.GetRow(TextBoxID), Grid.GetColumn(TextBoxID));

            }
            if (e.Key == Key.Escape)
            {

                TextBlock TextBlockID = new TextBlock();
                TextBlockID.MouseLeftButtonDown += TxtBlockID_MouseLeftButtonDown;
                TextBlockID.Text = pretext;
                Grid.SetRow(TextBlockID, Grid.GetRow(TextBoxID));
                Grid.SetColumn(TextBlockID, Grid.GetColumn(TextBoxID));
                DynamicGrid.Children.Remove(TextBoxID);
                DynamicGrid.Children.Add(TextBlockID);
            }
        }




    }
}
