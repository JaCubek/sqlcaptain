﻿using System.Collections.ObjectModel;
using System.Windows;
using System.ComponentModel;
using SQLCaptain.Views;
using System.Windows.Input;
using SQLCaptain.Models;
using System.Windows.Controls;

namespace SQLCaptain.Viewmodels
{


    public class MainViewModel : INotifyPropertyChanged
    {

        //public MainModel Action { get; set; } = new MainModel();
        //public ObservableCollection<string> AccountActions { get; private set; }
        public string titlename { get; set; }

        public MainViewModel()
        {
         /*   Action.AccountActions = new ObservableCollection<string>{
                 "Ustawienia",
                  "Wyloguj",
              };
            Action.BaseActions = new ObservableCollection<string>{
                  "Produkty",
                  "Klienci",
              };
              */

            LoadHomePageCommand = new BaseCommand(LoadHomePageView);
            LoadUserPreferencesCommand = new BaseCommand(LoadUserPreferencesView);
            LoadProductsCommand = new BaseCommand(LoadProductsView);
            LoadClientsCommand = new BaseCommand(LoadClientsView);
            LogOutCommand = new RelayCommand(LogOut);

            SelectedViewModel = new HomePageViewModel();

            DBModel ConnectionModel = new DBModel();

            titlename = "SQL Captain - jesteś zalogowany jako "+ ConnectionModel.loginname();
        }


        public ICommand LoadHomePageCommand { get; set; }
        public ICommand LoadUserPreferencesCommand { get; set; }
        public ICommand LoadProductsCommand { get; set; }
        public ICommand LogOutCommand { get; set; }
        public ICommand LoadClientsCommand { get; set; }

        private object selectedViewModel;

        public object SelectedViewModel

        {

            get { return selectedViewModel; }

            set { selectedViewModel = value; OnPropertyChanged("SelectedViewModel"); }

        }

        private void LogOut()
        {
            LoginView loginwindow = new LoginView();
            loginwindow.Show();
            Application.Current.MainWindow.Close();
            Application.Current.MainWindow = loginwindow;
        }

        private void LoadHomePageView(object obj)

        {

            SelectedViewModel = new HomePageViewModel();

        }

        private void LoadUserPreferencesView(object obj)

        {

            SelectedViewModel = new UserPreferencesViewModel();

        }

        private void LoadProductsView(object obj)

        {

            SelectedViewModel = new ProductsViewModel();

        }
        private void LoadClientsView(object obj)

        {
            DBModel ConnectionModel = new DBModel();

            if (ConnectionModel.IsAccessPossible())
            {
                SelectedViewModel = new ClientsViewModel();
            }else
            {
                MessageBox.Show("Brak uprawnień!");
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propName)

        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));

        }

        /*
         
          private void OnPropertyChanged(string propName)

        {

            if (PropertyChanged != null)

            {

                PropertyChanged(this, new PropertyChangedEventArgs(propName));

            }

        }

         */

    }


}
