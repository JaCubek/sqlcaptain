﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SQLCaptain.Models
{
    class DBModel
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;

        //Constructor
        public DBModel()
        {
            Initialize();
        }

        //Initialize values
        private void Initialize()
        {
            server = "localhost";
            database = "sys";
            uid = "userbazy";
            password = "dusysagu";
            string connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        MessageBox.Show("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        MessageBox.Show("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        #region Logowanie
        public bool LoginIn(string UserName, string UserPassword)
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                string query = "SELECT COUNT(1) FROM users WHERE UserName=@Username AND Password=@Password";
                MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.AddWithValue("@Username", UserName);
                sqlCmd.Parameters.AddWithValue("@Password", UserPassword);
                int count = Convert.ToInt32(sqlCmd.ExecuteScalar());

                if (count == 1)
                {
                    string query3 = "UPDATE users SET Active=0;";
                    MySqlCommand sqlCmd3 = new MySqlCommand(query3, connection);
                    sqlCmd3.ExecuteNonQuery();
                    string query2 = "UPDATE users SET Active=1 WHERE UserName=@Username;";
                    MySqlCommand sqlCmd2 = new MySqlCommand(query2, connection);
                    sqlCmd2.Parameters.AddWithValue("@Username", UserName);
                    sqlCmd2.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    MessageBox.Show("Login lub/i hasło są niepoprawne.");
                    return false;
                }
            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);
                return false;
            }
            finally
            {
                
                connection.Close();
                
            }

        }
        #endregion

        #region Zmiana hasła
        public bool UserChangePassword(string currentpassword, string newpassword)
        {

            try
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                string query = "SELECT COUNT(1) FROM users WHERE Active=1 AND Password=@Password";
                MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                sqlCmd.CommandType = CommandType.Text;
                sqlCmd.Parameters.AddWithValue("@Password", currentpassword);
                int count = Convert.ToInt32(sqlCmd.ExecuteScalar());

                if (count == 1)
                {
                    string query2 = "UPDATE users SET Password=@Password WHERE Active=001;";
                    MySqlCommand sqlCmd2 = new MySqlCommand(query2, connection);
                    sqlCmd2.Parameters.AddWithValue("@Password", newpassword);
                    sqlCmd2.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);
                return false;
            }
            finally
            {

                connection.Close();

            }
        }
        #endregion

        #region Pobranie tabeli produktów
        public List<DBProductsModel> GetProducts(string filterID, string filterName, string filterQuantity, string filterPrice, string orderBy, string orderHow)
        {

            List<DBProductsModel> products_list = new List<DBProductsModel>();


            string query = "SELECT * FROM products ";
            bool earlierFilter = false;

            if (!String.IsNullOrEmpty(filterID) || !String.IsNullOrEmpty(filterName) || !String.IsNullOrEmpty(filterQuantity) || !String.IsNullOrEmpty(filterPrice))
            {
                query += "WHERE";

                if (!String.IsNullOrEmpty(filterID))
                {
                    query += " product_id=@filterID";
                    earlierFilter = true;
                }
                if (!String.IsNullOrEmpty(filterName))
                {
                    if (earlierFilter == true)
                    {
                        query += " AND product_name=@filterName";
                        
                    }
                    else
                    {
                        query += " product_name=@filterName";
                        earlierFilter = true;
                    }
                    
                }
                if (!String.IsNullOrEmpty(filterQuantity))
                {
                    if (earlierFilter == true)
                    {
                        query += " AND product_quantity=@filterQuantity";

                    }
                    else
                    {
                        query += " product_quantity=@filterQuantity";
                        earlierFilter = true;
                    }

                }
                if (!String.IsNullOrEmpty(filterPrice))
                {
                    if (earlierFilter == true)
                    {
                        query += " AND product_price=@filterPrice";

                    }
                    else
                    {
                        query += " product_price=@filterPrice";
                        earlierFilter = true;                    
                    }

                }


            }

            query += " ORDER BY " + orderBy + " " + orderHow;
            

            try
            {
                using (connection)
                {

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                   // MySqlCommand sqlCmd = new MySqlCommand("SELECT * FROM products ORDER BY @orderBy @orderHow", connection);
                   // MySqlCommand sqlCmd = new MySqlCommand("SELECT * FROM products ORDER BY "+orderBy+" "+orderHow, connection);
                    MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                    sqlCmd.Parameters.AddWithValue("@filterID", filterID);
                    sqlCmd.Parameters.AddWithValue("@filterName", filterName);
                    sqlCmd.Parameters.AddWithValue("@filterQuantity", filterQuantity);
                    sqlCmd.Parameters.AddWithValue("@filterPrice", filterPrice);

                    MySqlDataReader reader = sqlCmd.ExecuteReader();
                    if (reader == null)
                    {
                        return null;
                    }

                    if (reader.HasRows)
                    {    
                        while (reader.Read())
                        {
                            products_list.Add(new DBProductsModel {
                                product_id=reader.GetInt32(0),
                                product_name=reader.GetString(1),
                                product_quantity=reader.GetInt32(2),
                                product_price=reader.GetDecimal(3)                               
                                });
                           
                        }
                        return products_list;
                        
                    }
                    else
                    {
                        return null;
                    }
                }


            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);
                return null;
            }
            finally
            {
                
                connection.Close();
               
            }
        }
        #endregion

        #region Pobranie tabeli klientów
        public List<DBClientsModel> GetClients()
        {

            List<DBClientsModel> clients_list = new List<DBClientsModel>();

            try
            {

                using (connection)
                {

                    MySqlCommand command = new MySqlCommand(
                      "SELECT * FROM clients",
                      connection);
                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    MySqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            clients_list.Add(new DBClientsModel
                            {
                                client_id = reader.GetInt32(0),
                                client_name = reader.GetString(1),
                                client_address = reader.GetString(2),
                                client_phone = reader.GetString(3)
                            });

                        }
                        return clients_list;
                    }
                    else
                    {
                        return null;
                    }


                }


            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);
                return null;
            }
            finally
            {

                connection.Close();

            }
        }
        #endregion

        #region Aktualizacja komórki produkty

        public void UpdateProductsCell(string newvalue, string id, int column)
        {

            try
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();
                string query="";
                //  string query = "UPDATE products SET product_name='HP Latex' WHERE product_id=4;";
                switch (column)
                {
                    case 1:
                         query = "UPDATE products SET product_name=@newvalue WHERE product_id=@id;";
                        break;
                    case 2:
                         query = "UPDATE products SET product_quantity=@newvalue WHERE product_id=@id;";
                        break;
                    case 3:
                         query = "UPDATE products SET product_price=@newvalue WHERE product_id=@id;";
                        break;
                }
                
                    MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                sqlCmd.Parameters.AddWithValue("@newvalue", newvalue);
                sqlCmd.Parameters.AddWithValue("@id", id);
                


                sqlCmd.ExecuteNonQuery();
                MessageBox.Show("Zaktualizowana!"); 

            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);

            }
            finally
            {

                connection.Close();

            }
            //MessageBox.Show(newvalue, row.ToString() + column.ToString() );
        }
        #endregion

        #region Aktualizacja komórki klienci
        public void UpdateClientsCell(string newvalue, int row, int column)
        {
            string kolumna = "";
            switch (column)
            {
                case 1:
                    kolumna = "client_name";
                    break;
                case 2:
                    kolumna = "client_address";
                    break;
                case 3:
                    kolumna = "client_phone";
                    break;
            }
            try
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                // string query = "UPDATE clients SET " + kolumna + "=@newvalue WHERE client_id=@row;";
                string query = "UPDATE clients SET "+kolumna+"=@newvalue WHERE client_id=@row;";
                MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                sqlCmd.Parameters.AddWithValue("@newvalue", newvalue);
                sqlCmd.Parameters.AddWithValue("@row", row);
                sqlCmd.ExecuteNonQuery();


            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);

            }
            finally
            {

                connection.Close();

            }
            //MessageBox.Show(newvalue, row.ToString() + column.ToString() );
        }
        #endregion

        #region Sprawdzenie poziomu dostępu
        public bool IsAccessPossible()
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                string query = "SELECT * FROM users WHERE Active=1";
                MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                MySqlDataReader reader = sqlCmd.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                     if (reader.GetInt32(0) == 1) return true;

                   // MessageBox.Show(reader.ToString());
                   // return true;
                }

                return false;

            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);
                return false;
            }
            finally
            {

                connection.Close();
              
            }
        }
        #endregion

        #region Nazwa aktualnie zalogowanego użytkownika
        public string loginname()
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                string query = "SELECT UserName FROM users WHERE Active=1";
                MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                MySqlDataReader reader = sqlCmd.ExecuteReader();

                if (reader.HasRows)
                {
                    reader.Read();
                    
                }
                return reader.GetString(0);
            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);
                return "";
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region Usuń rekord z tabeli produkty
        public bool deleteProduct(string id)
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                string query = "DELETE from products WHERE product_id=@id";
                MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                sqlCmd.Parameters.AddWithValue("@id", id);
                sqlCmd.ExecuteNonQuery();
                return true;


            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region Dodaj rekord do tabeli produkty
        public bool AddProduct(string name, string quantity, string price)
        {
            try
            {
                if (connection.State == ConnectionState.Closed)
                    connection.Open();

                string query = "INSERT into products SET product_name=@name, product_quantity=@quantity, product_price=@price";
                MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                sqlCmd.Parameters.AddWithValue("@name", name);
                sqlCmd.Parameters.AddWithValue("@quantity", quantity);
                sqlCmd.Parameters.AddWithValue("@price", price);
                sqlCmd.ExecuteNonQuery();
                return true;


            }
            catch (MySqlException Ex)
            {
                MessageBox.Show(Ex.Message);
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion
    }
}
