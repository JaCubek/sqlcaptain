﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLCaptain.Models
{
    class DBClientsModel
    {
        public int client_id { get; set; }
        public string client_name { get; set; }
        public string client_address { get; set; }
        public string client_phone{ get; set; }
    }
}

