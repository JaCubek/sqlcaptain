﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLCaptain.Models
{
    public class MainModel
    {

        public ObservableCollection<string> AccountActions { get; set; }= new ObservableCollection<string>();
        public ObservableCollection<string> BaseActions { get; set; } = new ObservableCollection<string>();
      
    }
}
