﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLCaptain.Models
{
    public class DBProductsModel
    {
        public int product_id { get; set; }
        public string product_name { get; set; }
        public int product_quantity { get; set; }
        public decimal product_price { get; set; }
    }
}
