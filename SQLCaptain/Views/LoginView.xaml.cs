﻿using System;
using System.Windows;
using System.Data;
using MySql.Data.MySqlClient;
using SQLCaptain.Models;

namespace SQLCaptain.Views
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginView : Window
    {


        

        public LoginView()
        {
            InitializeComponent();           

        }



        private void Login_Button_Click(object sender, RoutedEventArgs e)
        {

            DBModel ConnectionModel = new DBModel();
           bool logged = ConnectionModel.LoginIn(username_textbox.Text, password_textbox.Password);
            if (logged)
            {
                MainView mainwindow = new MainView();
                mainwindow.Show();
                Application.Current.MainWindow.Close();
                Application.Current.MainWindow = mainwindow;
            }
           
        }



            /*  private string server,database,uid,password,connectionString;

              private MySqlConnection connection;

                  private void Login_Button_Click(object sender, RoutedEventArgs e)
              {

                  server = "localhost";
                  database = "sys";
                  uid = "userbazy";
                  password = "dusysagu";

                  connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                  database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";

                  connection = new MySqlConnection(connectionString);

                  try
                  {
                      if (connection.State == ConnectionState.Closed)
                      connection.Open();

                      string query = "SELECT COUNT(1) FROM users WHERE UserName=@Username AND Password=@Password";
                      MySqlCommand sqlCmd = new MySqlCommand(query, connection);
                      sqlCmd.CommandType = CommandType.Text;
                      sqlCmd.Parameters.AddWithValue("@Username", username_textbox.Text);
                      sqlCmd.Parameters.AddWithValue("@Password", password_textbox.Password);
                      int count = Convert.ToInt32(sqlCmd.ExecuteScalar());

                      if (count == 1)
                      {
                          string query3 = "UPDATE users SET Active=0;";
                          MySqlCommand sqlCmd3 = new MySqlCommand(query3, connection);
                          sqlCmd3.ExecuteNonQuery();
                          string query2 = "UPDATE users SET Active=1 WHERE UserName=@Username;";                 
                          MySqlCommand sqlCmd2 = new MySqlCommand(query2, connection);
                          sqlCmd2.Parameters.AddWithValue("@Username", username_textbox.Text);
                          sqlCmd2.ExecuteNonQuery();
                          MainView mainwindow = new MainView();
                          mainwindow.Show();
                          this.Close();
                          Application.Current.MainWindow = mainwindow;

                      }
                      else
                      {
                          MessageBox.Show("Login lub/i hasło są niepoprawne.");
                      }
                  }
                  catch(MySqlException Ex)
                  {
                      MessageBox.Show(Ex.Message);
                  }
                  finally
                  {

                      connection.Close();
                  }

              }*/
        }
}
