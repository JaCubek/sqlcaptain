﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SQLCaptain.Models;

namespace SQLCaptain.Views
{
    /// <summary>
    /// Interaction logic for UserPreferencesView.xaml
    /// </summary>
    public partial class UserPreferencesView : UserControl
    {
        public UserPreferencesView()
        {
            InitializeComponent();
        }

        private void UserChangePassword_Button_Click(object sender, RoutedEventArgs e)
        {
            DBModel ConnectionModel = new DBModel();
            bool changed = ConnectionModel.UserChangePassword(currentpassword_textbox.Password, newpassword_textbox.Password);
            if (changed)
            {
                MessageBox.Show("Hasło zaktualizowane!");
                currentpassword_textbox.Clear();
                newpassword_textbox.Clear();
            }
            else
            {
                MessageBox.Show("Niepoprawne dane!");
                currentpassword_textbox.Clear();
                newpassword_textbox.Clear();
            }

        }
    }
}
